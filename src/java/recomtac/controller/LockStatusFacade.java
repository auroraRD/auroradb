/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import recomtac.entities.LockStatus;

/**
 *
 * @author Luis Caamaño
 */
@Stateless
public class LockStatusFacade extends AbstractFacade<LockStatus> {

    @PersistenceContext(unitName = "TestAuroraDB4PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LockStatusFacade() {
        super(LockStatus.class);
    }
    
}
