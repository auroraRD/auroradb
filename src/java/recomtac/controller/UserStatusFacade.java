/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.controller;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import recomtac.entities.UserStatus;

/**
 *
 * @author Luis Caamaño
 */
@Stateless
public class UserStatusFacade extends AbstractFacade<UserStatus> {

    @PersistenceContext(unitName = "TestAuroraDB4PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserStatusFacade() {
        super(UserStatus.class);
    }
    
}
