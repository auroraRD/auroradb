/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"LocationStart\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocationStart.findAll", query = "SELECT l FROM LocationStart l")
    , @NamedQuery(name = "LocationStart.findByDatetimeloc", query = "SELECT l FROM LocationStart l WHERE l.datetimeloc = :datetimeloc")
    , @NamedQuery(name = "LocationStart.findByPlace", query = "SELECT l FROM LocationStart l WHERE l.place = :place")
    , @NamedQuery(name = "LocationStart.findByCoords", query = "SELECT l FROM LocationStart l WHERE l.coords = :coords")
    , @NamedQuery(name = "LocationStart.findByType", query = "SELECT l FROM LocationStart l WHERE l.type = :type")
    , @NamedQuery(name = "LocationStart.findByName", query = "SELECT l FROM LocationStart l WHERE l.name = :name")
    , @NamedQuery(name = "LocationStart.findByDescr", query = "SELECT l FROM LocationStart l WHERE l.descr = :descr")
    , @NamedQuery(name = "LocationStart.findById", query = "SELECT l FROM LocationStart l WHERE l.id = :id")})
public class LocationStart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "datetimeloc")
    private String datetimeloc;
    @Column(name = "place")
    private String place;
    @Column(name = "coords")
    private String coords;
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "descr")
    private String descr;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "client", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client client;
    @OneToMany(mappedBy = "origin")
    private List<Job> jobList;

    public LocationStart() {
    }

    public LocationStart(Integer id) {
        this.id = id;
    }

    public LocationStart(Integer id, String name, String descr) {
        this.id = id;
        this.name = name;
        this.descr = descr;
    }

    public String getDatetimeloc() {
        return datetimeloc;
    }

    public void setDatetimeloc(String datetimeloc) {
        this.datetimeloc = datetimeloc;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCoords() {
        return coords;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @XmlTransient
    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocationStart)) {
            return false;
        }
        LocationStart other = (LocationStart) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.LocationStart[ id=" + id + " ]";
    }
    
}
