/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"Lock\"")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lock.findAll", query = "SELECT l FROM Lock l")
    , @NamedQuery(name = "Lock.findByNavixyid", query = "SELECT l FROM Lock l WHERE l.navixyid = :navixyid")
    , @NamedQuery(name = "Lock.findByJointechid", query = "SELECT l FROM Lock l WHERE l.jointechid = :jointechid")
    , @NamedQuery(name = "Lock.findByPhone", query = "SELECT l FROM Lock l WHERE l.phone = :phone")
    , @NamedQuery(name = "Lock.findByModel", query = "SELECT l FROM Lock l WHERE l.model = :model")
    , @NamedQuery(name = "Lock.findByDescription", query = "SELECT l FROM Lock l WHERE l.description = :description")
    , @NamedQuery(name = "Lock.findById", query = "SELECT l FROM Lock l WHERE l.id = :id")})
public class Lock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "navixyid")
    private String navixyid;
    @Column(name = "jointechid")
    private String jointechid;
    @Column(name = "phone")
    private String phone;
    @Column(name = "model")
    private String model;
    @Column(name = "description")
    private String description;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private LockStatus status;
    @OneToMany(mappedBy = "lock")
    private List<Job> jobList;

    public Lock() {
    }

    public Lock(Integer id) {
        this.id = id;
    }

    public String getNavixyid() {
        return navixyid;
    }

    public void setNavixyid(String navixyid) {
        this.navixyid = navixyid;
    }

    public String getJointechid() {
        return jointechid;
    }

    public void setJointechid(String jointechid) {
        this.jointechid = jointechid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LockStatus getStatus() {
        return status;
    }

    public void setStatus(LockStatus status) {
        this.status = status;
    }

    @XmlTransient
    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lock)) {
            return false;
        }
        Lock other = (Lock) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.Lock[ id=" + id + " ]";
    }
    
}
