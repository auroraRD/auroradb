/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"Job\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Job.findAll", query = "SELECT j FROM Job j")
    , @NamedQuery(name = "Job.findByName", query = "SELECT j FROM Job j WHERE j.name = :name")
    , @NamedQuery(name = "Job.findById", query = "SELECT j FROM Job j WHERE j.id = :id")
    , @NamedQuery(name = "Job.findByCat", query = "SELECT j FROM Job j WHERE j.cat = :cat")
    , @NamedQuery(name = "Job.findByCud", query = "SELECT j FROM Job j WHERE j.cud = :cud")
    , @NamedQuery(name = "Job.findByCreated", query = "SELECT j FROM Job j WHERE j.created = :created")
    , @NamedQuery(name = "Job.findByModified", query = "SELECT j FROM Job j WHERE j.modified = :modified")})
public class Job implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "cat")
    private String cat;
    @Column(name = "cud")
    private String cud;
    @Column(name = "created")
    @Temporal(TemporalType.DATE)
    private Date created;
    @Column(name = "modified")
    @Temporal(TemporalType.DATE)
    private Date modified;
    @JoinColumn(name = "client", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client client;
    @JoinColumn(name = "container", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Container container;
    @JoinColumn(name = "driver", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobAgent driver;
    @JoinColumn(name = "responsible", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobAgent responsible;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobStatus status;
    @JoinColumn(name = "type", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobType type;
    @JoinColumn(name = "destination", referencedColumnName = "id")
    @ManyToOne
    private LocationEnd destination;
    @JoinColumn(name = "origin", referencedColumnName = "id")
    @ManyToOne
    private LocationStart origin;
    @JoinColumn(name = "lock", referencedColumnName = "id")
    @ManyToOne
    private Lock lock;
    @JoinColumn(name = "user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User user;

    public Job() {
    }

    public Job(Integer id) {
        this.id = id;
    }

    public Job(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getCud() {
        return cud;
    }

    public void setCud(String cud) {
        this.cud = cud;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Container getContainer() {
        return container;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    public JobAgent getDriver() {
        return driver;
    }

    public void setDriver(JobAgent driver) {
        this.driver = driver;
    }

    public JobAgent getResponsible() {
        return responsible;
    }

    public void setResponsible(JobAgent responsible) {
        this.responsible = responsible;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public LocationEnd getDestination() {
        return destination;
    }

    public void setDestination(LocationEnd destination) {
        this.destination = destination;
    }

    public LocationStart getOrigin() {
        return origin;
    }

    public void setOrigin(LocationStart origin) {
        this.origin = origin;
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Job)) {
            return false;
        }
        Job other = (Job) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.Job[ id=" + id + " ]";
    }
    
}
