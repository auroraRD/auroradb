/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"JobAgentRole\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobAgentRole.findAll", query = "SELECT j FROM JobAgentRole j")
    , @NamedQuery(name = "JobAgentRole.findById", query = "SELECT j FROM JobAgentRole j WHERE j.id = :id")
    , @NamedQuery(name = "JobAgentRole.findByName", query = "SELECT j FROM JobAgentRole j WHERE j.name = :name")
    , @NamedQuery(name = "JobAgentRole.findByDescription", query = "SELECT j FROM JobAgentRole j WHERE j.description = :description")})
public class JobAgentRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
    private List<JobAgent> jobAgentList;

    public JobAgentRole() {
    }

    public JobAgentRole(Integer id) {
        this.id = id;
    }

    public JobAgentRole(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<JobAgent> getJobAgentList() {
        return jobAgentList;
    }

    public void setJobAgentList(List<JobAgent> jobAgentList) {
        this.jobAgentList = jobAgentList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobAgentRole)) {
            return false;
        }
        JobAgentRole other = (JobAgentRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.JobAgentRole[ id=" + id + " ]";
    }
    
}
