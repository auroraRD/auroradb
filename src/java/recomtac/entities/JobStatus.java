/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity

@Table(name = "public.\"JobStatus\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobStatus.findAll", query = "SELECT j FROM JobStatus j")
    , @NamedQuery(name = "JobStatus.findByName", query = "SELECT j FROM JobStatus j WHERE j.name = :name")
    , @NamedQuery(name = "JobStatus.findBySeqnum", query = "SELECT j FROM JobStatus j WHERE j.seqnum = :seqnum")
    , @NamedQuery(name = "JobStatus.findByDescription", query = "SELECT j FROM JobStatus j WHERE j.description = :description")
    , @NamedQuery(name = "JobStatus.findById", query = "SELECT j FROM JobStatus j WHERE j.id = :id")})
public class JobStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "seqnum")
    private int seqnum;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<Job> jobList;

    public JobStatus() {
    }

    public JobStatus(Integer id) {
        this.id = id;
    }

    public JobStatus(Integer id, String name, int seqnum, String description) {
        this.id = id;
        this.name = name;
        this.seqnum = seqnum;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeqnum() {
        return seqnum;
    }

    public void setSeqnum(int seqnum) {
        this.seqnum = seqnum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobStatus)) {
            return false;
        }
        JobStatus other = (JobStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.JobStatus[ id=" + id + " ]";
    }
    
}
