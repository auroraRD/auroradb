/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"Client\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findById", query = "SELECT c FROM Client c WHERE c.id = :id")
    , @NamedQuery(name = "Client.findByName", query = "SELECT c FROM Client c WHERE c.name = :name")
    , @NamedQuery(name = "Client.findByDescription", query = "SELECT c FROM Client c WHERE c.description = :description")
    , @NamedQuery(name = "Client.findByType", query = "SELECT c FROM Client c WHERE c.type = :type")
    , @NamedQuery(name = "Client.findByAddress", query = "SELECT c FROM Client c WHERE c.address = :address")
    , @NamedQuery(name = "Client.findByDatecreated", query = "SELECT c FROM Client c WHERE c.datecreated = :datecreated")
    , @NamedQuery(name = "Client.findByLogin", query = "SELECT c FROM Client c WHERE c.login = :login")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private String type;
    @Column(name = "address")
    private String address;
    @Column(name = "datecreated")
    private String datecreated;
    @Basic(optional = false)
    @Column(name = "login")
    private String login;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<JobAgent> jobAgentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<LocationStart> locationStartList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<LocationEnd> locationEndList;
    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne
    private ClientStatus status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private List<Job> jobList;

    public Client() {
    }

    public Client(Integer id) {
        this.id = id;
    }

    public Client(Integer id, String name, String description, String login) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @XmlTransient
    public List<JobAgent> getJobAgentList() {
        return jobAgentList;
    }

    public void setJobAgentList(List<JobAgent> jobAgentList) {
        this.jobAgentList = jobAgentList;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<LocationStart> getLocationStartList() {
        return locationStartList;
    }

    public void setLocationStartList(List<LocationStart> locationStartList) {
        this.locationStartList = locationStartList;
    }

    @XmlTransient
    public List<LocationEnd> getLocationEndList() {
        return locationEndList;
    }

    public void setLocationEndList(List<LocationEnd> locationEndList) {
        this.locationEndList = locationEndList;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }

    @XmlTransient
    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.Client[ id=" + id + " ]";
    }
    
}
