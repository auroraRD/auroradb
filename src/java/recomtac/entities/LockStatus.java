/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"LockStatus\"")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LockStatus.findAll", query = "SELECT l FROM LockStatus l")
    , @NamedQuery(name = "LockStatus.findByName", query = "SELECT l FROM LockStatus l WHERE l.name = :name")
    , @NamedQuery(name = "LockStatus.findByDescription", query = "SELECT l FROM LockStatus l WHERE l.description = :description")
    , @NamedQuery(name = "LockStatus.findById", query = "SELECT l FROM LockStatus l WHERE l.id = :id")})
public class LockStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
    private List<Lock> lockList;

    public LockStatus() {
    }

    public LockStatus(Integer id) {
        this.id = id;
    }

    public LockStatus(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public List<Lock> getLockList() {
        return lockList;
    }

    public void setLockList(List<Lock> lockList) {
        this.lockList = lockList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LockStatus)) {
            return false;
        }
        LockStatus other = (LockStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.LockStatus[ id=" + id + " ]";
    }
    
}
