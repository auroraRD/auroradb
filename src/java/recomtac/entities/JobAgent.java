/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recomtac.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Caamaño
 */
@Entity
@Table(name = "public.\"JobAgent\"")

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobAgent.findAll", query = "SELECT j FROM JobAgent j")
    , @NamedQuery(name = "JobAgent.findByFirstname", query = "SELECT j FROM JobAgent j WHERE j.firstname = :firstname")
    , @NamedQuery(name = "JobAgent.findByLastname", query = "SELECT j FROM JobAgent j WHERE j.lastname = :lastname")
    , @NamedQuery(name = "JobAgent.findByDocumentid", query = "SELECT j FROM JobAgent j WHERE j.documentid = :documentid")
    , @NamedQuery(name = "JobAgent.findByEmail", query = "SELECT j FROM JobAgent j WHERE j.email = :email")
    , @NamedQuery(name = "JobAgent.findByCellphone", query = "SELECT j FROM JobAgent j WHERE j.cellphone = :cellphone")
    , @NamedQuery(name = "JobAgent.findById", query = "SELECT j FROM JobAgent j WHERE j.id = :id")})
public class JobAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "firstname")
    private String firstname;
    @Basic(optional = false)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @Column(name = "documentid")
    private String documentid;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "cellphone")
    private String cellphone;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "client", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client client;
    @JoinColumn(name = "role", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobAgentRole role;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "driver")
    private List<Job> jobList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "responsible")
    private List<Job> jobList1;

    public JobAgent() {
    }

    public JobAgent(Integer id) {
        this.id = id;
    }

    public JobAgent(Integer id, String firstname, String lastname, String documentid, String email, String cellphone) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.documentid = documentid;
        this.email = email;
        this.cellphone = cellphone;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDocumentid() {
        return documentid;
    }

    public void setDocumentid(String documentid) {
        this.documentid = documentid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public JobAgentRole getRole() {
        return role;
    }

    public void setRole(JobAgentRole role) {
        this.role = role;
    }

    @XmlTransient
    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    @XmlTransient
    public List<Job> getJobList1() {
        return jobList1;
    }

    public void setJobList1(List<Job> jobList1) {
        this.jobList1 = jobList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobAgent)) {
            return false;
        }
        JobAgent other = (JobAgent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "recomtac.entities.JobAgent[ id=" + id + " ]";
    }
    
}
